﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Outlook = Microsoft.Office.Interop.Outlook; 

namespace DayLightMonitor
{
  class Program
  {
    static void Main(string[] args)
    {
      Outlook.Application app = new Outlook.Application();
      Outlook.AppointmentItem appt = app.CreateItem(
              Outlook.OlItemType.olAppointmentItem)
              as Outlook.AppointmentItem;
      appt.Subject = "Sunset";
      Outlook.RecurrencePattern pattern = appt.GetRecurrencePattern();
      pattern.RecurrenceType = Outlook.OlRecurrenceType.olRecursYearly;
      // Logical OR for DayOfWeekMask creates pattern
      pattern.PatternStartDate = DateTime.Today;
      pattern.StartTime = DateTime.Parse("16:13");
      pattern.Duration = 0;
      appt.ReminderMinutesBeforeStart = 5;
      appt.Save();
    }
  }
}
